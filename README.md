# Soy SPS+

## Secciones Principales
- Noticias
- Obras
- Yo Reporto
- Consultas

## Estructura de Aplicacion
* app
    * assets: mantiene todos los assets estaticos del proyecto
    * components: contiene los componentes reusables del proyecto.
    * const: constantes usadas como urls y apis
    * screens: componentes de cada una de las funcionalidades de las secciones. 
        * news: contiene componentes el detalle de vistas
        * receipt: contiene componentes para el detalle de recibos
        * report: contiene vista para formulario y vista de tipo modal.
        * works: contiene vistas de lista de distritos, vista de lista y detalle de obras
    * styles: estilos generales del proyecto

## Archivos principales

