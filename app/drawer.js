import React, { Component } from 'react';
import { View } from 'react-native';
import Drawer from 'react-native-drawer';
import {Actions, DefaultRenderer} from 'react-native-router-flux';
import SideMenu from './components/drawer/sidemenu_v2.js';


export default class extends Component {
  render(){
      const state = this.props.navigationState;
      const children = state.children;
      return (
          <SideMenu
            changeLang={this.props.changeLang.bind(this)}
          />
      );
  }
}
