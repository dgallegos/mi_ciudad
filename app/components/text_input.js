import React from 'react';
import { View, StyleSheet, Text, TextInput } from 'react-native'

class InputText extends React.Component{
  render(){
    const {
      containerStyles,
      labelContainerStyles,
      inputContainerStyles,
      labelStyles,
      inputStyles,
      value,
      label,
      onChange,
      placeholder,
      placeholderTextColor,
      multiline,
      numberOfLines = 1,
      editable=true,
      ...props
    } = this.props; 
      return(
        <View style={[styles.container, containerStyles || {}]}>
          {  label && 
            <View style={[styles.labelContainer, labelContainerStyles || {}]}>
              <Text style={[styles.label, labelStyles || {}]}>
                {label}
              </Text>
            </View>
          }
          <View style={[styles.inputContainer, inputContainerStyles || {}]}>
            <TextInput
              style={[
                styles.input, {
                  height: numberOfLines !== 1 ? numberOfLines * 14 + 40 : 40
                }, 
                inputStyles || {}, 
              ]}
              onChangeText={onChange}
              value={value}
              placeholder={placeholder}
              placeholderTextColor={placeholderTextColor}
              underlineColorAndroid={"transparent"}
              autoCorrect={false}
              multiline={multiline}
              numberOfLines={numberOfLines}
              editable={editable}
              {...props}
              ref={(r) => props.hasOwnProperty('onRef') ? props.onRef(r) : null}
            />
          </View>
        </View>
      )
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection:'row'
  },
  labelContainer: {
    flex: 2,
    paddingTop: 11,
    paddingLeft: 10
  },
  label: {
    fontSize: 14,
    fontWeight: '500'
  },
  inputContainer : {
    flex: 1
  },
  input: {
    flex: 1,
    height: 40,
    fontSize: 14,
  }
});

export default InputText
