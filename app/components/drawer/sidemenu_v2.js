import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, Dimensions, ListView } from 'react-native';
import {Actions} from 'react-native-router-flux'
import DrawerCard from './drawer-card.js';
import * as themeColors from '../../styles/colors.js';
import EStyleSheet from 'react-native-extended-stylesheet';


//Languaje
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

let menuItems= [
  {img: () => require('../../assets/icons/noticia-icon-2x.png'), onPress: ()=> Actions.news(), label: () => I18n.t('news')},
  {img: () => require('../../assets/icons/obra-icon-2x.png'), onPress: ()=> Actions.works(), label: () => I18n.t('works')},
  {img: () => require('../../assets/icons/reporto-icon-2x.png'), onPress: ()=> Actions.report(), label: () => I18n.t('i_report')},
  {img: () => require('../../assets/icons/consulta-icon-2x.png'), onPress: ()=> Actions.receipt(), label: () => I18n.t('queries')}
];


class SideMenu extends React.Component{
  constructor(props){
    super(props)
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2)=> r1 != r2})
    this.state={
      ds: this.ds.cloneWithRows(menuItems)
    }
  }

  render(){
    const {changeLang} = this.props;
    return(
      <View style={styles.drawer}>
        {/*Background image + mask*/}
        <View style={{position:'absolute',top: 0,left: 0,bottom: 0,right: 0}}>
          <Image style={{resizeMode:"cover", width: Dimensions.get('window').width, height: Dimensions.get('window').height}} source={require("../../assets/foto-sps-xs.png")}>
            <View style={{flex:1,backgroundColor:"rgba(247,146,30,0.7)"}}></View>
          </Image>
        </View>

        <View style={{position:'absolute', justifyContent:'center', flex: 1, top: 20, left: Dimensions.get('window').width * 0.5 - 22}}>
          <Image style={{width: 45, height: 45, resizeMode: 'contain'}} source={require('../../assets/alcaldia-logo/alcaldia-4x.png')}></Image>
        </View>
        <View style={{flex: 1, alignItems: 'center',marginTop: 70}}>
          {/*Title*/}
          <View style={{marginBottom: 30}}>
            <View style={styles.title}>
              <Text style={styles.titleText}>
                {I18n.t('your_city')}
              </Text>
            </View>
            <View style={styles.title}>
              <Text style={styles.titleText}>San Pedro Sula</Text>
            </View>
          </View>
          {/*icons*/}
          <ListView
            contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap', marginTop: 0, justifyContent: 'center', alignItems: 'center', paddingBottom:20}}
            style={{flex: 1}}
            dataSource={this.state.ds}
            enableEmptySections={true}
            renderRow={row => <DrawerCard {...row}/>}
          />
        </View>
        <View style={{alignItems: 'center'}}>
          <Image style={{width: 80, height: 30, resizeMode: 'contain', marginBottom: 10}} source={require('../../assets/obras-logo/obras-4x.png')}></Image>
        </View>
        {/*Lang*/}
        <View style={[styles.row, {position: 'relative'}]}>
          <TouchableOpacity
            style={{flex: 1, backgroundColor: 'rgb(247,146,30)' }}
            onPress={()=> changeLang.bind(this)("es")}
          >
            <View>
              <Text style={styles.buttonText}>Español</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.triangleCorner}/>
          <TouchableOpacity
            style={{flex: 1, backgroundColor: '#fff'}} 
            onPress={()=> changeLang.bind(this)("en")}
          >
            <View>
              <Text style={[styles.buttonText, {color: "black"}]}>English</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      );
    }
};

const styles = EStyleSheet.create({
  drawer:{
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'transparent'
  },
  row:{
    flexDirection: 'row'
  },
  titleText:{
    fontSize: 26,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'Gotham'
  },
  title: {
    paddingTop: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingBottom: 5  },
  buttonText:{
    color: '#fff',
    fontSize: 20,
    padding: 15,
    textAlign: 'center',
    fontFamily: 'Gotham'
  },
  triangleCorner: {
    position: 'absolute',
    zIndex: 2,
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: 30,
    borderTopWidth: 60,
    borderRightColor: 'transparent',
    borderTopColor: 'rgb(247,146,30)'
  },
})

export default SideMenu;
