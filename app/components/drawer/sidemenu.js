import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import {Actions} from 'react-native-router-flux'
import DrawerCard from './drawer-card.js';
import * as themeColors from '../../styles/colors.js';


//Languaje
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

let menuItems= [
  {icon: 'newspaper-o', color: themeColors.secondary},
  {icon: 'bullhorn', color: themeColors.secondary},
  {icon: 'map-marker', color: themeColors.secondary},
  {icon: 'search', color: themeColors.secondary}
];


const SideMenu = (props) =>(
  <View style={styles.drawer}>
    <View style={styles.title}>
      <Text style={[styles.titleText,{ marginRight: 5, fontWeight: 'bold'}]}>{I18n.t('your_city')}</Text>
      <Text style={styles.titleText}>SPS</Text>
    </View>
    <View style={styles.row}>
      <DrawerCard {...menuItems[0]} label={I18n.t('news')} onPress={()=>{
        Actions.news();
      }}/>
      <DrawerCard {...menuItems[1]} label={I18n.t('works')} onPress={()=>{
        Actions.works();
      }}/>
    </View>
    <View style={styles.row}>
      <DrawerCard {...menuItems[2]} label={I18n.t('i_report')} onPress={()=>{
        Actions.report();
      }}/>
      <DrawerCard {...menuItems[3]} label={I18n.t('queries')} onPress={()=>{
        Actions.receipt();
      }}/>
    </View>
    <TouchableOpacity style={{borderBottomWidth: 1,borderTopWidth: 1, borderColor: themeColors.darkBorder}}
      onPress={()=>props.changeLang.bind(this)("es")}>
      <View>
        <Text style={styles.buttonText}>Español</Text>
      </View>
    </TouchableOpacity>
    <TouchableOpacity style={styles.darkBorder}
      onPress={()=>props.changeLang.bind(this)("en")}>
      <View>
        <Text style={styles.buttonText}>English</Text>
      </View>
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  drawer:{
    flex: 1,
    backgroundColor: themeColors.primary
  },
  row:{
    flexDirection: 'row'
  },
  darkBorder:{
    borderTopWidth: 1,
    borderColor: themeColors.darkBorder,
    borderBottomWidth: 1
  },
  titleText:{
    fontSize: 24,
    color: themeColors.accentText,
    textAlign: 'center'
  },
  title: {
    backgroundColor: themeColors.accent,
    paddingTop: 30,
    borderBottomWidth: 4,
    borderColor: themeColors.secondary,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingBottom: 5
  },
  buttonText:{
    color: '#fff',
    fontSize: 16,
    padding: 20,
    textAlign: 'center',
    fontWeight: '600'
  }
})

export default SideMenu;
