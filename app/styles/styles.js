import { StyleSheet } from 'react-native';
import  * as  themeColors  from './colors.js';

const styles = StyleSheet.create({
  card: {
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center', 
    height: 160, 
    borderWidth: 1, 
    borderColor: themeColors.darkBorder,
    
  }
});

export default styles;