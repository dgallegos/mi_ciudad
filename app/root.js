import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StatusBar,AsyncStorage, NativeModules, PixelRatio, Dimensions, Image, Platform} from 'react-native';
import Drawer from './drawer.js';
import {Actions, Scene, Router, ActionConst, Modal} from 'react-native-router-flux';
import  * as  themeColors  from './styles/colors.js';
// Screens
import News from './screens/news.js';
import Works from './screens/works.js';
import Report from './screens/report.js';
import Receipt from './screens/receipt.js';
import MyDistrict from './screens/works/my_district.js';

import ReportForm from './screens/report/report-form.js';
import ReportModal from './screens/report/report-modal.js';
import WorkDetail from './screens/works/work-detail.js';
import NewsDetail from './screens/news/news_detail.js';
import ReceiptDetail from './screens/receipt/receipt_detail.js';

//Language
import I18n from 'react-native-i18n'
import Lang from './const/lang'
I18n.fallbacks = true
I18n.translations= Lang;
import EStyleSheet from 'react-native-extended-stylesheet';
let {height, width} = Dimensions.get('window');

const Title = ({text, image}) =>(
  <View style={{flexDirection:'row', justifyContent: 'center', alignItems: 'center', marginTop: Platform.OS=='android' ? 10 : 20}}>
    <View style={{marginRight: 10}}>
      <Image source={image()} style={{resizeMode:'contain',width: 35, height: 35}}/>
    </View>
    <View>
     <Text style={{fontFamily: 'Gotham', textAlign: 'center',color: '#fff', fontSize: 22, fontWeight: '400'}}>{text()}</Text>
    </View>
  </View>
);


EStyleSheet.build({
    rem: width > 340 ? 18 : 14
});

export default class App extends Component{
  constructor(props){
    super(props);
    // Select lang, between en and es.
    this.selectLang()
    I18n.locale=this.lang
    this.state={
      lang: this.lang
    }
  }

  selectLang(){
    if(/^es*/.test(I18n.currentLocale())){
      this.lang = 'es'
    }else if(/^en*/.test(I18n.currentLocale())){
      this.lang = 'en'
    }else{
      this.lang = 'es'
    }
  }

  componentDidMount(){
    StatusBar.setHidden(true);
  }

  changeLang(lang){
    AsyncStorage.setItem("lang", lang).then(()=>{
       this.setState({lang:null})
       I18n.locale= lang || 'es';
       this.setState({lang});
    });
  }

  render(){
    if(this.state.lang !=null){
      return (
        <Router>
          <Scene key="modal" component={Modal}>

            <Scene key='root'>
              <Scene
                key='main'
                component={Drawer}
                initial={true}
                changeLang={this.changeLang.bind(this)}
                passProps={true}
                hideNavBar
              />
                {/*Main screens*/}
                <Scene
                  key='news'
                  component={News}
                  renderTitle={()=><Title text={()=>I18n.t('news')} image={()=>require('./assets/icons/noticia-icon-2x.png')}/>}
                  titleStyle={{color: '#fff'}}
                  navigationBarStyle={{backgroundColor: '#265fa6'}}
                  hideNavBar={false}
                  backButtonImage={require('./assets/chevron-right.png')}
                />
                <Scene
                  key='works'
                  component={Works}
                  renderTitle={()=><Title text={()=>I18n.t('works')} image={()=>require('./assets/icons/obra-icon-2x.png')}/>}
                  navigationBarStyle={{backgroundColor: 'rgb(0,184,255)', borderWidth: 0}}
                  hideNavBar={false}
                  backButtonImage={require('./assets/chevron-right.png')}
                />
                <Scene
                  key='receipt'
                  component={Receipt}
                  renderTitle={()=><Title text={()=>I18n.t('queries')} image={()=>require('./assets/icons/consulta-icon-2x.png')}/>}
                  navigationBarStyle={{backgroundColor: "rgb(247,146,30)", borderWidth: 0}}
                  hideNavBar={false}
                  backButtonImage={require('./assets/chevron-right.png')}
                />
                <Scene
                  key='report'
                  component={Report}
                  titleStyle={{color: '#fff'}}
                  renderTitle={()=><Title text={()=>I18n.t('i_report')} image={()=>require('./assets/icons/reporto-icon-2x.png')}/>}
                  navigationBarStyle={{backgroundColor: 'rgb(193,39,45)'}}
                  hideNavBar={false}
                  backButtonImage={require('./assets/chevron-right.png')}
                />
                {/*Detail Views*/}
                <Scene
                  component={NewsDetail}
                  hideNavBar
                  key="news_detail"
                  direction="horizontal"
                  navigationBarStyle={{height: 0}}
                />
                <Scene
                  component={WorkDetail}
                  hideNavBar
                  key="work_detail"
                  direction="horizontal"
                  navigationBarStyle={{height: 0}}
                />
                <Scene
                  key='my_district'
                  title={I18n.t('district')}
                  component={MyDistrict}
                  direction="horizontal"
                  hideNavBar={false}
                  navigationBarStyle={{backgroundColor: 'rgb(0,184,255)'}}
                  titleStyle={{color: '#fff'}}
                  backButtonImage={require('./assets/chevron-right.png')}
                />
                <Scene
                  key='report_form'
                  component={ReportForm}
                  renderTitle={()=><Title text={()=>I18n.t('i_report')} image={()=>require('./assets/icons/reporto-icon-2x.png')}/>}
                  navigationBarStyle={{backgroundColor: 'rgb(193,39,45)'}}
                  backButtonImage={require('./assets/chevron-right.png')}
                  hideNavBar={false}
                />
                <Scene
                  key="receipt_detail"
                  direction="vertical"
                  component={ReceiptDetail}
                  hideNavBar
                />
                <Scene
                  key="report_modal"
                  direction="vertical"
                  component={ReportModal}
                  title="Modal"
                  hideNavBar
                />
            </Scene>
          </Scene>
        </Router>
      )
    }else{
      return(<View/>)
    }
  }
}
