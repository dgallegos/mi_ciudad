import React, {Component} from 'react';
import { View, Text, TouchableOpacity, ListView, StyleSheet, Image, ScrollView } from 'react-native';
import {Actions} from 'react-native-router-flux';
import TextInput from '../../components/text_input.js';
import Button from '../../components/button.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import Parallax from 'react-native-parallax';
import HTMLView from 'react-native-htmlview';
import moment from "moment";
import 'moment/locale/es';
import * as Apis from "../../const/apis"
//Languaje
import I18n from 'react-native-i18n'


export default class NewsDetail extends Component{
  constructor(props){
    super(props)
    this.carousellDS = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2,});
    this.state={
      imageIndex:0,
    }
  }


  changeUriMain(imageIndex){
    this.setState({imageIndex})
  }

  createThumb(elt,key){
    return(
      <TouchableOpacity
        key={key}
        style={{height: 75}}
        onPress={()=>{this.changeUriMain(key)} }
      >
        <Image style={{height: 75, width: 130}} source={{uri:`${Apis.backoffice}${elt.path}`}} />
      </TouchableOpacity>
    )
  }


  render(){
    moment.locale(I18n.locale)
    const {data} = this.props;
    const {imageIndex: idx} = this.state
    var dataContent=data.content_english;
    if(I18n.locale=="es"){
      dataContent=data.content
    }

    return (
      <View style={{flex: 1, justifyContent: 'center', backgroundColor: 'rgba(151,151,151,.1)'}}>
        <ScrollView>
          <View style={{borderColor: 'rgba(151,151,151,.1)',paddingTop:20,backgroundColor: '#265fa6'}}>
            <TouchableOpacity 
              style={{justifyContent: 'flex-end', alignItems: 'flex-end', marginRight: 17, padding: 7, paddingHorizontal: 10}}
              onPress={()=> Actions.pop()}
            >
              <Image style={{resizeMode:"contain", width: 16, height: 16}} source={require("../../assets/close-icon.png")}/>
            </TouchableOpacity>
            <View style={{flex: 1, marginHorizontal: 10, padding: 10, borderColor: 'rgba(151,151,151,.1)'}}>
              <Text style={{fontFamily: 'Gotham', color: '#fff', fontSize: 14}}>{I18n.locale=="es"? data.category.name : data.category.name_english}</Text>
              <View style={{flex:1,alignItems:"flex-start"}}>
                  <Text style={{fontFamily: 'Gotham', color: '#fff', fontSize: 16, fontWeight: '700'}}>{I18n.locale=="es"? data.title : data.title_english}</Text>
              </View>
              <View style={{justifyContent:"space-between",marginTop:15,bottom:1}}>
                <Text style={{fontFamily: 'Gotham', color: '#fff', fontSize: 12,fontWeight: '600'}}>{I18n.t('created_by')} : {data.user.full_name}</Text>
                <Text style={{fontFamily: 'Gotham', color: '#fff', fontSize: 12}}>{moment(data.created_at).format('ddd D MMM YYYY h:mm a')}</Text>
              </View>
              <View style={{flex:1,alignItems:"flex-start",borderBottomWidth:1,borderColor: '#fff',paddingBottom:5}}/>
            </View>
          </View>
          <View style={{height: 280, padding: 0}}>
            <Image source={{uri:`${Apis.backoffice}${data.images.hasOwnProperty(idx) ? data.images[idx].path : null }`}} style={{width: null, height: 280}} resizeMode='cover'></Image>
          </View>
          <View style={{borderBottomWidth: 1, borderColor: 'rgba(0,0,0,.2)', backgroundColor: 'rgba(128,128,128,.1)'  }}>
            <ScrollView
              ref={(scrollView) => { _scrollView = scrollView; }}
              automaticallyAdjustContentInsets={false}
              scrollEventThrottle={200}
              horizontal={true}
              style={{height: 75}}>
              {data.images.map((elt, key) => this.createThumb(elt, key))}
            </ScrollView>
          </View>

          <View style={{flex: 1, flexDirection: 'row', backgroundColor: 'rgba(128,128,128,.1)' }}>
            <View style={{flex: 1, marginHorizontal: 10, padding: 10,}}>

              <HTMLView value={`<div>${dataContent}</div>`} stylesheet={HtmlStyle}/>
            </View>

          </View>
        </ScrollView>
      </View>
    )
  }
}

var bodyColor="#7a7f84"
const HtmlStyle = StyleSheet.create({
  div:{
    marginHorizontal:10,
    fontFamily: 'Gotham',
    
  },
  strong:{
    color:bodyColor
  },
  p:{
    color:bodyColor
  },
  li:{
    color:bodyColor
  },
  blockquote:{
    color:bodyColor
  }
})

const styles = StyleSheet.create({
  iconContainer: {
    justifyContent: 'center',
    margin: 10,
    borderWidth: 5,
    height: 95,
    width: 95,
    borderRadius: 100
  }
})
