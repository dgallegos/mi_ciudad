import React from 'react';
import { View, Text, TouchableOpacity, ListView, StyleSheet,Platform, Switch, Alert, Image, Dimensions, ScrollView, NetInfo} from 'react-native';
import {Actions} from 'react-native-router-flux';
import TextInput from '../components/text_input.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import Button from '../components/button.js';
import { Bars } from 'react-native-loader';
import BaseComponent from '../components/BaseComponent.js';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import * as Apis from "../const/apis";
import I18n from 'react-native-i18n'
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

class Receipt extends BaseComponent{
  initialState={
    codigo_catastral: '',
    rmc_empresarial:'',
    disabled: false,
    switch: false,
  }

  constructor(){
    super();
    this.state= this.initialState;
    this._bind('onSubmit','onChange', 'onSubmitCheckConnection')
  }

  reset(){
    this.setState({...this.initialState});
  }

  onChange(field, value){
    this.setState({
      [field]: value
    })
  }

  validate(){
    // Both empty
    if(this.state.codigo_catastral == '' && this.state.rmc_empresarial == ''){
      Alert.alert(I18n.t('data_empty'), null ,[{
        text: 'Aceptar'
      }]);
      return false;
    }

    return true;
  }


  onSubmit(){
    const body = {
      codigo_catastral: this.state.codigo_catastral,
      rmc_empresarial: this.state.rmc_empresarial
    };

    if(!this.validate()){
      return;
    }

    this.setState({
      disabled: true
    })

    fetch(`${Apis.backoffice}api/receipt`,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify(body)
    })
    .then(res => {
      if(!res.ok){
        console.log(res.statusText)
        return;
      }
      return res.json();
    })
    .catch(err => console.log(err))
    .then(data => {
      console.log(data);

      let total = 0;
      if(data.bienes != null){
        total= parseFloat(data.bienes.SaldosAlaFecha) + parseFloat(data.servicios.SaldosAlaFecha)
        if(data.bienes.CodigoRetorno == "0"){
          Alert.alert(I18n.t('cadastral_code_not_found'), null ,[{
            text: 'Aceptar'
          }]);
          this.setState({
            disabled: false
          });
          return;
        }
      }

      if(data.negocios != null){
        total += parseFloat(data.negocios.SaldosAlaFecha);
        if(data.negocios.CodigoRetorno == "0"){
          Alert.alert(I18n.t('rmc_not_found'), null ,[{
            text: 'Aceptar'
          }]);
          this.setState({
            disabled: false
          });
          return;
        }
      }

      Actions.receipt_detail({
        ...this.state,
        ...data,
        total: parseFloat(total.toFixed(2))
      });

      this.setState({
        disabled: false
      })
    })
    .catch(err => console.log(err))
  }
  
  onSubmitCheckConnection(){
    NetInfo.isConnected.fetch().done(
      isConnected => { 
        if(isConnected){
          this.onSubmit();
        }else{
          Alert.alert(I18n.t('no_connection'), null ,[{
            text: 'Aceptar'
          }]);
        }
        this.setState({isConnected}); 
      }
    );
  }


  render(){
    return(
      <View style={{flex: 1,  backgroundColor: '#EEEEEE', marginTop:((Platform.OS=='android')?50:64), justifyContent: 'center', alignItems: 'center'}}>
        <View style={{position:'absolute',top: 0,left: 0,bottom: 0,right: 0}}>
          <Image style={{resizeMode:"cover", width: Dimensions.get('window').width, height: Dimensions.get('window').height}} source={require("../assets/consultas.png")}>
            <View style={{flex:1,backgroundColor:"rgba(247,146,30,0.7)"}}></View>
          </Image>
        </View>
        {/*Form*/}
        <ScrollView style={{flex: 1}}>
          <View style={{flex: 1, marginVertical: 70, justifyContent: 'center', alignItems: 'stretch'}}>
            <View style={{marginBottom: 40}}>
              <Text style={{fontFamily: 'Gotham', textAlign: 'center', color: '#fff', fontSize: 22, backgroundColor: 'transparent'}}>
                {I18n.t('receipt_instructions')}
              </Text>
            </View>
            <TextInput
              value={this.state.codigo_catastral}
              onChange={text => this.onChange('codigo_catastral',text)}
              placeholder={I18n.t('cadastral_code')}
              placeholderTextColor='#979797'
              inputStyles={styles.input}
              containerStyles={styles.inputContainer}
            />
            <TextInput
              value={this.state.rmc_empresarial}
              onChange={text => this.onChange('rmc_empresarial',text)}
              placeholder={I18n.t('rmc')}
              placeholderTextColor='#979797'
              inputStyles={styles.input}
              containerStyles={styles.inputContainer}
            />
          </View>
          {/*Button*/}
          <Button
            style={[styles.button, {backgroundColor: 'transparent'}]}
            onPress={this.onSubmit}
            disabled={this.state.disabled}
            >
            <Text style={styles.buttonText}>{I18n.t('generate_detail')}</Text>
          </Button>
          <View style={styles.loader}>
          {
            this.state.disabled ?
              <Bars size={15} color="#fff" />
            : null
          }
          </View>
          <KeyboardSpacer topSpacing={50}/> 
        </ScrollView>
    </View>
    )
  }

}

const styles = StyleSheet.create({
  input:{
    textAlign: 'center',
    marginLeft: 15,
    backgroundColor: '#fff',
    borderRadius: 3,
    paddingLeft: 10,
    fontFamily: 'Gotham'
  },
  inputContainer:{
    marginHorizontal: 25,
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#089CC5',
    marginHorizontal: 40,
    padding: 10,
    marginTop: 30, 
    marginBottom: 30
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 24,
    fontWeight: 'normal',
    fontFamily: 'Gotham'
  },
  loader: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'transparent'
  }
})

export default Receipt;