import React, {Component} from 'react';
import { View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import Button from '../../components/button.js';
//Languaje
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

const ReportModal = (props) => (
  <View style={{flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.5)', alignItems: 'center'}}>
    <View style={{backgroundColor: '#fff', marginHorizontal: 40, marginVertical: 120}}>
      <View style={{height: 40, backgroundColor: 'rgb(193,39,45)'}}>
      </View>
      <View style={{marginVertical: 15, marginHorizontal: 5, alignItems: 'center'}}>
        {
          props.error && 
            <Icon name='close' color='rgb(193,39,45)' size={50}/>
        }
        {
          !props.error &&
            <Icon name='check-circle' color='rgb(193,39,45)' size={50}/>
        }
        <Text style={{fontFamily: 'Gotham', textAlign: 'center', marginTop: 5}}>
          {
            props.error &&
              I18n.t('ticket_feedback_error')
          }
          {
            !props.error &&
              I18n.t('ticket_feedback')
          }
        </Text>
        <Text style={{fontFamily: 'Gotham', marginTop: 10}}>
          # Ticket: {props.ticket}
        </Text>
        <Button 
          style={{backgroundColor: 'rgb(193,39,45)', height: 35, marginTop: 10, marginHorizontal: 10, marginBottom: 15}}
          onPress={()=>props.error ? Actions.pop(): Actions.pop({popNum:2})}
        >
          <Text style={{fontFamily: 'Gotham', color: 'white', marginVertical: 10, textAlign: 'center', paddingHorizontal: 20}}>{I18n.t('accept')}</Text>
        </Button>
      </View>
    </View>
  </View>
)

export default ReportModal;