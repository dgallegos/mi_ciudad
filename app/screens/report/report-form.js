import React from 'react';
import { View, Text, TouchableOpacity, ListView, StyleSheet, ScrollView, Image, Alert, Dimensions, Platform, ImageEditor, ImageStore, NetInfo, findNodeHandle } from 'react-native';
import {Actions} from 'react-native-router-flux';
import TextInput from '../../components/text_input.js';
import Button from '../../components/button.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import {v4} from 'react-native-uuid';
import BaseComponent from '../../components/BaseComponent.js';
import { Bars } from 'react-native-loader';
import * as Apis from "../../const/apis";
import ImageResizer from 'react-native-image-resizer';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import RNFS from 'react-native-fs';
import dismissKeyBoard from 'dismissKeyboard'
//Languaje
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

 const options = {
   title: 'Seleccionar Imagen',
   quality: 0,
   storageOptions: {
     skipBackup: true,
     path: 'images',
     waitUntilSaved: true
   },
   cameraType: 'back',
   takePhotoButtonTitle: 'Tomar Foto',
   cancelButtonTitle: 'Cancelar',
   chooseFromLibraryButtonTitle: 'Seleccionar Imagen',
 };

 const validateEmail= (email)=> {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

export default class ReportForm extends BaseComponent{
  constructor(){
    super();
    this.state = {
      name: '',
      email: '',
      message: '',
      attachments: [],
      subject: '',
      status: '',
      // source: 'Gobierno Electronico App',
      disabled: false
    }
    this._bind('select', 'onSubmit','onSubmitCheckConnection');
  }

  componentDidMount(){
    this.setState({
      subject: this.props.iconInfo.name()
    });
  }

  renderImages(){
    let { attachments: images } = this.state;
    return (
      <View style={{flexDirection:'column', alignItems: 'center'}}>
        <ScrollView
          automaticallyAdjustContentInsets={false}
          horizontal={true}
          style={{width:300,margin:5}}
        >
          {
            images.map(img =>
              <TouchableOpacity key={img.id} style={{margin: 3}} onPress={() => this.imageStatus(img.id)} >
                <Image style={{ height:50, width:50, marginRight: 5}} source={{uri: img.uri}} />
              </TouchableOpacity>
            )
          }
        </ScrollView>
      </View>
    )
  }

  imageStatus(id){
    const imgOptions = {
      ...options,
      customButtons: [{
        name:'delete' ,
        title:'Eliminar'
      }]
    };

    this.pickImage(imgOptions, 'edit', id);
  }

  select(){
    if(this.state.attachments.length == 2){
      Alert.alert('Solo puedes enviar dos imagenes', null ,[{
        text: 'Aceptar'
      }]);
      return;
    }
    this.pickImage(options, 'add');
  }

  pickImage(options, action, id = null){
    let { attachments: images } = this.state;

    ImagePicker.showImagePicker(options, response => {
      if(response.customButton == 'delete'){
        let index = images.findIndex(x=> x.id==id)
        images.splice(index,1);
        this.setState({ attachments: images });
      }else{

        if(response.error){
          return;
        }

        
        if (!response.didCancel) {
          const image = 'data:image/png;base64,' + response.data;
          ImageResizer.createResizedImage(image, 320, 480, "JPEG",50).then((resizedImageUri) => {
            // resizeImageUri is the URI of the new image that can now be displayed, uploaded...
            const rawPath = resizedImageUri;

            Image.getSize(rawPath, (width,height)=>{
              const cropData = {
                offset:{
                  x:0,
                  y:0
                },
                size: {
                  width,
                  height
                }
              };

              ImageEditor.cropImage(
                rawPath,
                cropData, 
                (uri) => {
                  ImageStore.getBase64ForTag(
                    uri, 
                    (base64)=>{
                      const source = {uri: 'data:image/png;base64,' + base64, isStatic: true};

                      if(action === 'add'){
                        source.id = v4();
                        images.push(source);
                      }

                      if(action === 'edit'){
                        let index = images.findIndex(x=> x.id==id)
                        images[index].uri = source.uri;
                      }

                      this.setState({ attachments: images });

                      // ImageStore.removeImageForTag(uri)
                    },
                    (err) => console.log(err)
                  );
                }, 
                (err) => console.log(err)
              );
            });
          }).catch((err) => {
            console.log(err)
          });
          
        }
      }
    });
  }

  processImages(){
    let images = this.state.attachments.map(image => {
      return {[`${image.id}-${(new Date()).getTime()}.png`]: image.uri}
    });

    return images
  }

  validate(){
    const {name, email, message} = this.state;
    if(name == ''){
      Alert.alert(I18n.t('empty_name'), null ,[{
        text: 'Aceptar'
      }]);
      return false;
    }

    if(email == ''){
      Alert.alert(I18n.t('empty_email'), null ,[{
        text: 'Aceptar'
      }]);
      return false;
    }

    if(message == ''){
      Alert.alert(I18n.t('empty_message'), null ,[{
        text: 'Aceptar'
      }]);
      return false;
    }

    if(message.length < 5){
      Alert.alert(I18n.t('length'), null ,[{
        text: 'Aceptar'
      }]);
      return false;
    }

    if(!validateEmail(email)){
      Alert.alert(I18n.t('valid_email'), null ,[{
        text: 'Aceptar'
      }]);
      return false;
    }

    return true;
  }

  onSubmit(){
    if(!this.validate()){
      return
    }
    const {point} = this.props;
    let pointMessage = this.state.message + ` (${point.lat},${point.lng})`;
    this.setState({
      disabled: true
    })

    const attachments = this.processImages();
    const body = {
      ...this.state,
      message: pointMessage,
      attachments
    };

    fetch(`${Apis.backoffice}api/tickets`,{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        "Content-type":"application/json"
      },
      body: JSON.stringify(body)
    })
    .then((response) =>{
      if(!response.ok){
        return;
      }
      return response.json()
    })
    .then((data) => {
      this.setState({
        disabled: false
      });
      
      var reg = /Unable/i;
      console.log(data)
      Actions.report_modal({ticket: data.message, error: reg.test(data.message)})
    })
    .catch( (err) => {
      console.log(err)
      Alert.alert(I18n.t('error'), null ,[{
        text: 'Aceptar'
      }])
      }
    )
  }

  onSubmitCheckConnection(){
    NetInfo.isConnected.fetch().done(
      isConnected => { 
        if(isConnected){
          this.onSubmit();
        }else{
          Alert.alert(I18n.t('no_connection'), null ,[{
            text: 'Aceptar'
          }]);
        }
        this.setState({isConnected}); 
      }
    );
  }

  render(){
    const {iconInfo} = this.props;
    
    return (
      <View style={styles.container}>
        {/*Bg + mask*/}
        <View style={{position:'absolute',top: 0,left: 0,bottom: 0,right: 0}}>
          <Image style={{resizeMode:"cover", width: Dimensions.get('window').width, height: Dimensions.get('window').height}} source={require("../../assets/foto-sps-xs.png")}>
            <View style={{flex:1,backgroundColor:'rgba(193,39,45,0.8)'}}></View>
          </Image>
        </View>
        
        {/*Form*/}
        <ScrollView style={{flex: 1}}>
          {/*icon + name*/}
          <View style={styles.typeContainer}>
            <View style={{margin: 5}}>
              <Image style={{resizeMode: 'contain', width: 80, height: 90}} source={iconInfo.icon()}/>
            </View>
            <View style={{backgroundColor: 'transparent'}}>
              <Text style={{fontFamily: 'Gotham',fontSize: 18, textAlign: 'center', color: '#fff'}}>{iconInfo.name()}</Text>
            </View>
          </View>
          {/*Form*/}
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <TextInput
              ref='name'
              value={this.state.name}
              onChange={text => this.setState({name: text})}
              placeholder={I18n.t("name")}
              placeholderTextColor='#979797'
              inputStyles={styles.input}
              containerStyles={styles.inputContainer}
              returnKeyType='next'
              blurOnSubmit={false}
              onSubmitEditing={()=>this.email.focus()}
            />
            <TextInput
              ref='email'
              value={this.state.email}
              onChange={text => this.setState({email: text.toLowerCase()})}
              placeholder={I18n.t("email")}
              placeholderTextColor='#979797'
              inputStyles={styles.input}
              containerStyles={styles.inputContainer}
              onRef={(r) => this.email = r}
              blurOnSubmit={false}
              returnKeyType='next'
              onSubmitEditing={()=>this.message.focus()}
            />
            <TextInput
              value={this.state.message}
              onChange={text => this.setState({message: text})}
              placeholder={I18n.t("comments")}
              placeholderTextColor='#979797'
              inputStyles={styles.input}
              containerStyles={styles.inputContainer}
              numberOfLines={2}
              multiline
              onRef={(r) => this.message = r}
            />
          </View> 
          {/*Images*/}       
          { this.renderImages()}
          {/*Camera + Submit*/}
          <View style={{flexDirection: 'row', alignItems:"center",justifyContent:"center", marginTop: 30}}>
            <TouchableOpacity
              style={{alignItems:"center",justifyContent:"center"}}
              onPress={()=>this.select()}
            >
              <Icon style={{textAlign:'center', backgroundColor:'transparent', color: '#fff'}} size={30} name="camera" />
            </TouchableOpacity>
            <Button
              style={{backgroundColor: 'transparent', height: 50, marginHorizontal: 35, alignItems:"center",justifyContent:"center"}}
              onPress={this.onSubmit}
              disabled={this.state.disabled}
            >
              <Text style={{fontFamily: 'Gotham',color: 'white', marginVertical: 10, textAlign: 'center', fontSize: 22}}>{I18n.t("report")}</Text>
            </Button>

          </View>

          {/*Loader*/}
          <View style={styles.loader}>
            {
              this.state.disabled ?
                <Bars size={15} color="#fff" />
              : null
            }
          </View>          
          <KeyboardSpacer topSpacing={50}/>  
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EEEEEE',
    marginTop: (Platform.OS=='android')?50:64
  },
  typeContainer:{
    marginVertical: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconContainer: {
    justifyContent: 'center',
    top:10,
    margin: 15,
    borderWidth: 5,
    height: 50,
    width: 50,
    borderRadius: 100
  },
  icon: {
    textAlign: 'center',
    backgroundColor: 'transparent',
    fontFamily: 'Gotham'
  },
  input:{
    textAlign: 'left',
    marginLeft: 15,
    backgroundColor: '#fff',
    borderRadius: 3,
    paddingLeft: 10,
    fontFamily: 'Gotham'
  },
  inputContainer:{
    marginHorizontal: 25,
    marginBottom: 15,
  },
  loader: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  }
})