import React, {Component} from 'react';
import { View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const ReportIcon = ({name, icon, color}) => (
  <View>
    <TouchableOpacity 
      style={[styles.iconContainer, {borderColor: color}]}
    >
      <View style={{margin: 5}}>
        <Icon style={{textAlign: 'center'}} name={icon} size={30} color={color}/>
      </View>
    </TouchableOpacity>
    <View style={{justifyContent: 'center', alignItems: 'center', flexWrap: 'wrap'}}>
      <Text style={{fontSize: 12, textAlign: 'center', color: color, fontWeight: 'bold'}}>{name}</Text>
    </View>
  </View>
)

const styles = StyleSheet.create({
  iconContainer: {
    justifyContent: 'center', 
    margin: 10, 
    borderWidth: 5, 
    height: 95, 
    width: 95, 
    borderRadius: 100
  }
})

export default ReportIcon;