import React, {Component} from 'react';
import { View, Text, TouchableOpacity, ListView, StyleSheet, Alert } from 'react-native';
import {Actions} from 'react-native-router-flux';
import TextInput from '../../components/text_input.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {Bars} from 'react-native-loader';
import BaseComponent from '../../components/BaseComponent.js';
import * as Apis from "../../const/apis";

const validateEmail= (email)=> {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

//0790082000
class ReceiptDetail extends BaseComponent{
  constructor(){
    super();
    this.state = {
      email: '',
      loadingBar: false
    }
    this._bind('sendEmail');
  }


  sendEmail(){
    const {bienes, servicios, negocios, codigo_catastral, rmc_empresarial} = this.props;
    let body  = [];

    if(!validateEmail(this.state.email)){
      Alert.alert('No es un correo valido', null ,[{
        text: 'Aceptar'
      }]);
      return;
    }
    if(codigo_catastral != '')
      body = {
        bienes,
        servicios,
        negocios:{},
        rmc_empresarial: '',
        codigo_catastral,
        email: this.state.email
      }

    if(rmc_empresarial != '')
      body = {
        bienes:{},
        servicios:{},
        codigo_catastral:'',
        negocios,
        rmc_empresarial,
        email: this.state.email
      }
    
    this.setState({
      loadingBar: true
    });
    
    fetch(`${Apis.backoffice}api/receipt?flag=send`,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify(body)
    })
    .then(res => {
      return res.json();
    })
    .catch(err => console.log(err))
    .then(data => {
      this.setState({
        loadingBar: false
      });
      // console.log(data)
      if(data.message){
        Alert.alert('Se ha enviado correo', null ,[{
          text: 'Aceptar', onPress: () => Actions.pop()
        }]);

      }else{
        Alert.alert('Correo no se ha podido enviar', null ,[{
          text: 'Aceptar'
        }]);
      }
    })
    .catch(err => console.log(err))
  }

  render(){
    let {bienes, servicios, negocios, total, codigo_catastral, rmc_empresarial} = this.props
    console.log(this.props)
    if(bienes == null) [bienes, servicios] = [[],[]];
    if(negocios == null) negocios = [];
    return (
      <View style={styles.container}>
        <View style={{marginTop: 50}}>
          <Text style={{fontFamily: 'Gotham', textAlign: 'center', fontSize: 16}}>
          {codigo_catastral != '' && bienes.NombreContribuyente}
          {rmc_empresarial != '' && negocios.NombrenNegocio}
          </Text>
        </View>
        <View style={{marginTop: 5}}>
          <Text style={{fontFamily: 'Gotham', textAlign: 'center', fontSize: 14}}>Impuesto Municipales</Text>
        </View>
        <View style={styles.detailContainer}>
          {
            codigo_catastral != '' &&
            <View>
              <View style={styles.lineContainer}>
                <Text style={styles.descriptionText}>Bienes</Text>
                <Text style={styles.amountText}>{bienes.SaldosAlaFecha}</Text>
              </View>
              <View style={styles.lineContainer}>
                <Text style={styles.descriptionText}>Servicios</Text>
                <Text style={styles.amountText}>{servicios.SaldosAlaFecha}</Text>
              </View>
            </View>
          }
          {
            rmc_empresarial != '' &&
            <View style={styles.lineContainer}>
              <Text style={styles.descriptionText}>Industria y Comercio</Text>
              <Text style={styles.amountText}>{negocios.SaldosAlaFecha}</Text>
            </View>
          }
        </View>

        <View style={styles.sendEmail}>
          <TextInput
            value={this.state.email}
            onChange={email => this.setState({email: email.toLowerCase()})}
            placeholder="Correo Electronico"
            placeholderTextColor='#979797'
            inputStyles={styles.emailInput}
            containerStyles={styles.emailInputContainer}
          />

          <TouchableOpacity
            style={styles.envelope}
            onPress={this.sendEmail}
          >
            {
              this.state.loadingBar ?
                <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
                    <Bars size={10} color="#fff"/>
                </View> :
                <Icon color='#fff' name='envelope' size={20}/>
            }
          </TouchableOpacity>
        </View>

        <View style={styles.separator}>
        </View>

        <View style={styles.totalContainer}>
          <View style={styles.lineContainer}>
            <Text style={[styles.descriptionText, {fontSize: 16, fontWeight: '600'}]}>Total</Text>
            <Text style={[styles.amountText, {fontSize: 16}]}>Lps. {total.toFixed(4)}</Text>
          </View>
        </View>
        <KeyboardSpacer/>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EEEEEE',
    // marginTop: 64
  },
  detailContainer: {
    flex: 1,
    backgroundColor: '#fff',
    marginVertical: 50,
  },
  lineContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    paddingHorizontal: 25,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(151,151,151,0.5)'
  },
  descriptionText: {
    flex: 1,
    fontSize: 14,
    color: '#4A4A4A',
    fontFamily: 'Gotham'
  },
  amountText:{
    fontSize: 14,
    color: '#4A4A4A',
    fontFamily: 'Gotham'
  },
  sendEmail: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: 'rgba(151,151,151,0.5)'
  },
  emailInput: {
    textAlign: 'center',
    color: '#000',
    marginLeft: 10,
    fontFamily: 'Gotham'
  },
  emailInputContainer: {
    flex:1,
    backgroundColor: '#fff',
    borderTopColor: '#979797'
  },
  envelope: {
    backgroundColor: 'rgb(247,146,30)',
    justifyContent: 'center',
    paddingVertical: 7,
    paddingHorizontal: 20
  },
  separator: {
    backgroundColor: 'transparent',
    height: 50
  },
  totalContainer: {
    backgroundColor: '#fff'
  }
})

export default ReceiptDetail;
