import React, {Component} from 'react';
import { View, Text, TouchableOpacity, ListView, StyleSheet,Platform, Image, ScrollView, Dimensions} from 'react-native';
import {Actions} from 'react-native-router-flux';
import I18n from 'react-native-i18n'
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;


let types = [
  {name: () => I18n.t('bump'), icon: ()=>require('../assets/icons/bache-2x.png'), color: '#fff'},
  {name: () => I18n.t('garbage_truck'), icon: ()=>require('../assets/icons/ta-x2.png'), color: '#fff'},
  {name: () => I18n.t('waste_ground'), icon: ()=>require('../assets/icons/sb-2x.png'), color: '#fff'},
  {name: () => I18n.t('traffic_light'), icon: ()=>require('../assets/icons/sem-2x.png'), color: '#fff'},
  {name: () => I18n.t('traffic_jam'), icon: ()=>require('../assets/icons/cg-v-2x.png'), color: '#fff'},
  {name: () => I18n.t('vehicle_collision'), icon: ()=>require('../assets/icons/cv-2x.png'), color: '#fff'},
  {name: () => I18n.t('road_blocking'), icon: ()=>require('../assets/icons/tvp-2x.png'), color: '#fff'},
  {name: () => I18n.t('illegal_blocking'), icon: ()=>require('../assets/icons/cipv-2x.png'), color: '#fff'}
];

const ReportIcon = ({name, icon, color, point}) => (
  <View style={{alignItems: 'center', width: Dimensions.get('window').width * .45, margin: 0, padding: 0, height: 140}}>
    <TouchableOpacity
      onPress={() => Actions.report_form({iconInfo: {name, icon, color: '#000'}, point: point()})}
    >
      <View style={{margin: 5}}>
        <Image style={{resizeMode: 'contain', width: 80, height: 90}} source={icon()}/>
      </View>
    </TouchableOpacity>
    <View style={{backgroundColor: 'transparent'}}>
      <Text style={{fontFamily: 'Gotham', fontSize: 18, textAlign: 'center', color: '#fff'}}>{name()}</Text>
    </View>
  </View>
);

class Report extends Component{
  constructor(props){
    super(props)
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      ds: this.ds.cloneWithRows(types),
      initialPosition: 'unknown',
      lastPosition: 'unknown',
      point: {}
    };
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        var initialPosition = JSON.stringify(position);
        this.setState({
          initialPosition,
          point: {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          }
        });
      },
      (error) => console.log(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );
    this.watchID = navigator.geolocation.watchPosition((position) => {
      var lastPosition = JSON.stringify(position);
      this.setState({lastPosition});
    });
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }


  render(){
    return(
      <View style={{flex: 1, marginTop: (Platform.OS=='android') ? 50 : 64, backgroundColor: 'transparent', alignItems: 'center'}}>
        <View style={{position:'absolute',top: 0,left: 0,bottom: 0,right: 0}}>
          <Image style={{resizeMode:"cover", width: Dimensions.get('window').width, height: Dimensions.get('window').height}} source={require("../assets/foto-sps-xs.png")}>
            <View style={{flex:1,backgroundColor:'rgba(193,39,45,0.8)'}}></View>
          </Image>
        </View>
        <ListView
          contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap', marginTop: 20, paddingVertical: 30}}
          dataSource={this.state.ds}
          enableEmptySections={true}
          renderRow={ row => <ReportIcon {...row} point={()=>this.state.point}/>}
        />
      </View>
    )
  }

}

export default Report;
