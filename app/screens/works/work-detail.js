import React, {Component} from 'react';
import { View, Text, TouchableOpacity, ListView, StyleSheet, Image, ScrollView,AsyncStorage } from 'react-native';
import {Actions} from 'react-native-router-flux';
import TextInput from '../../components/text_input.js';
import Button from '../../components/button.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import MapView from 'react-native-maps';
import Carousel from '../../components/carousel'
import Modal from 'react-native-modalbox';
import _ from "lodash"
import update from 'react-addons-update'; // ES6

//Components
import * as Apis from "../../const/apis";
import formatCurrency from "../../const/formatCurrency";

//Languaje
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

import moment from "moment";
const Modall =({state,children})=>(
  <Modal
    position={"center"}
    style={{height:500}}
    isOpen={state}
    swipeToClose={false}
    backButtonClose={true}
    backdropPressToClose={true}
    >
    <View style={styles.modalView}>
      {children}
    </View>
  </Modal>

)

const WorkDetails =({data})=>(
  <View style={{flex: 1}}>
     <TouchableOpacity 
      style={{justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: 20, marginRight: 17, padding: 7, paddingHorizontal: 10}}
      onPress={()=> Actions.pop()}
    >
      <Image style={{resizeMode:"contain", width: 16, height: 16}} source={require("../../assets/close-icon.png")}/>
    </TouchableOpacity>
    <View style={{flex:1,backgroundColor:"transparent",justifyContent:"flex-end",alignItems:"flex-start",paddingLeft:30,paddingBottom:35}}>
      <Text style={{fontFamily: 'Gotham', color:"#fff",fontSize:15}}>{I18n.locale=="es"? data.name : data.name_english}</Text>
      <Text style={{fontFamily: 'Gotham', color:"#fff",fontSize:19,fontWeight:"600"}}>{`${data.neighborhood} ${data.route}`}</Text>
    </View>
  </View>
)

export default class WorkDetail extends Component{
  constructor(props){
    super(props)
    this.state={
      openModal:false,
      favorite:false
    }
  }

  componentDidMount(){
    const {data} = this.props
    AsyncStorage.getItem("works_favorites").then(wf=>{
      if(wf==null){
        AsyncStorage.setItem("works_favorites",JSON.stringify([]))
      }else{
        var isFav = _.find(JSON.parse(wf), ['id', data.id]);
        if(isFav){
          this.setState({favorite:isFav.favorite})
        }
      }
    })
  }

  favorite(){
    const {data} = this.props
    // console.log('data', data)
    AsyncStorage.getItem("works_favorites").then(wf=>{
      var newCol=[];

      var favorites = _.find(JSON.parse(wf), ['id', data.id]);
      var index = _.findIndex(JSON.parse(wf), ['id', data.id]);

      if(favorites){//si esta noticia es favorita edita
        favorites.favorite = !favorites.favorite
        newCol = update(JSON.parse(wf),
          {
            [index]: {
              $merge: {...favorites}
            }
          }
        );
        this.setState({favorite:favorites.favorite})
      }

      if(!favorites){//si no crea el registro de favorito

        var f = {id:data.id,favorite:true}
        newCol = update(JSON.parse(wf),{
          $push:[f]
        });
        this.setState({favorite:f.favorite})
      }

      AsyncStorage.setItem("works_favorites",JSON.stringify(newCol))
    });
  }

  render(){
    const {data} = this.props
    const {favorite} = this.state
    return (
      <View style={{flex: 1, justifyContent: 'center', backgroundColor: '#EEEEEE'}}>
        <Carousel
           dataImage={data.images}
           getImageFrom={"path"}
           joinFirst={Apis.backoffice}
           mainImageComponents={<WorkDetails data={data}/>}
        />
        <View style={{flexDirection: 'row',justifyContent:"space-between", borderBottomWidth: 1, borderColor: 'rgba(151,151,151,.1)'}}>
          <TouchableOpacity style={{flex: 1, flexDirection: 'row', padding: 17}}
            onPress={()=>this.favorite.bind(this)()}>
            <Icon name='heart-o' size={18} style={{marginRight: 8}} color={(favorite) ? "red":'#D1D1D1'}/>
            <Text style={{fontFamily: 'Gotham', color:'#9B9B9B', fontSize: 14}}>{I18n.t("favorite")}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=>this.setState({openModal:true})} style={{flex: 1, flexDirection: 'row', padding: 17}}>
            <Icon name='map-o' size={18} style={{marginRight: 8}} color='#D1D1D1'/>
            <Text style={{fontFamily: 'Gotham', color: '#9B9B9B', fontSize: 14}}>{I18n.t("map")}</Text>
          </TouchableOpacity>
        </View>

        <ScrollView
          ref={(scrollView) => { _scrollViewContent = scrollView; }}
          automaticallyAdjustContentInsets={false}
          onScroll={() => { console.log('onScroll!'); }}
          scrollEventThrottle={200}
          style={{height: 500}}>
          <View style={{flexDirection: 'row', borderBottomWidth: 1, borderColor: 'rgba(151,151,151,.1)'}}>
            <View style={{flex: 1, marginHorizontal: 20, borderRightWidth: 1, padding: 10, borderColor: 'rgba(151,151,151,.1)'}}>
              <Text style={{fontFamily: 'Gotham', color: 'rgb(0,184,255)', fontSize: 16, fontWeight: '600'}}>{I18n.t("start")}</Text>
              <Text style={{fontFamily: 'Gotham', color: 'rgb(0,184,255)', fontSize: 12}}>{moment(data.start).format('ddd D MMM YYYY')}</Text>
            </View>
            <View style={{flex: 1, marginHorizontal: 10, padding: 10}}>
              <Text style={{fontFamily: 'Gotham', color: 'rgb(0,184,255)', fontSize: 16, fontWeight: '600'}}>{I18n.t("end")}</Text>
              <Text style={{fontFamily: 'Gotham', color: 'rgb(0,184,255)', fontSize: 12}}>{moment(data.end).format('ddd D MMM YYYY')}</Text>
            </View>
          </View>

          <View style={{flexDirection: 'row', borderBottomWidth: 1, borderColor: 'rgba(151,151,151,.1)'}}>
            <View style={{flex: 1, marginHorizontal: 20, paddingVertical: 10, borderRightWidth: 1, padding: 10, borderColor: 'rgba(151,151,151,.1)'}}>
              <Text style={{fontFamily: 'Gotham', color: 'rgb(0,184,255)', fontSize: 16, fontWeight: '600'}}>{I18n.t("investment")}</Text>
              <Text style={{fontFamily: 'Gotham', color: 'rgb(0,184,255)', fontSize: 12}}>{I18n.locale=="es"? data.investment : data.investment_english}</Text>
            </View>
            <View style={{flex: 1, marginHorizontal: 10, paddingVertical: 10, padding: 10}}>
              <Text style={{fontFamily: 'Gotham', color: 'rgb(0,184,255)', fontSize: 16, fontWeight: '600'}}>{I18n.t("executor")}</Text>
              <Text style={{fontFamily: 'Gotham', color: 'rgb(0,184,255)', fontSize: 12}}>{data.contractor}</Text>
            </View>
          </View>

          <View style={{flex: 1, flexDirection: 'row', }}>
            <View style={{flex: 1, marginHorizontal: 20, padding: 10}}>
              <Text style={{fontFamily: 'Gotham', color: 'rgb(0,184,255)', fontSize: 16, fontWeight: '600', }}>{I18n.t("about_project")}</Text>
              <Text style={{fontFamily: 'Gotham', color: 'rgb(0,184,255)', fontSize: 12}}>{I18n.locale=="es"? data.description : data.description_english}</Text>
            </View>
          </View>
        </ScrollView>
        <Modall state={this.state.openModal}>
          <View style={{flex: 1}}>
           <MapView
              style={{flex: 1, height: 400}}
              region={{
                latitude: parseFloat(data.lat),
                longitude: parseFloat(data.lng),
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
            >
            <MapView.Marker
              coordinate={{ latitude: parseFloat(data.lat), longitude: parseFloat(data.lng),}}
            />
           </MapView>
          </View>
        </Modall>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  iconContainer: {
    justifyContent: 'center',
    margin: 10,
    borderWidth: 5,
    height: 95,
    width: 95,
    borderRadius: 100
  },
  modalView:{
    flex:1,
  },

})
