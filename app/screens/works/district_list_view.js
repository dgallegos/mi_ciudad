import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as themeColors from '../../styles/colors.js';
import {Actions} from 'react-native-router-flux';

import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;


const DistrictListView = ({data}) => (
  <TouchableOpacity
    style={{flex: 1, flexDirection: 'row', backgroundColor: '#fff', marginHorizontal: 20, marginVertical: 5, borderRadius: 5}}
    onPress={()=>Actions.my_district({districts_id:data.id, works: data.works})}>
    <View style={{flex: 1, margin: 15, justifyContent: 'center'}}>
      <Text style={{fontFamily: 'Gotham',fontSize: 18, marginBottom: 5}}>{ I18n.locale=="es" ? data.name : data.district_english}</Text>
      <View style={{flexDirection:'row'}}>
        <View>
          <Text style={{fontFamily: 'Gotham', color: '#2a61a8'}}>{I18n.t('in_progress')}: {data.onProgress}</Text>
        </View>
        <View style={{marginLeft: 10}}>
          <Text style={{fontFamily: 'Gotham', color: '#ec1c23'}}>{I18n.t('done')}: {data.done}</Text>
        </View>
      </View>
    </View>
    <View style={{justifyContent: 'center', margin: 10}}>
      <Icon style={{textAlign: 'center'}} name='chevron-right' size={40} color='#e6e6e6'/>
    </View>
  </TouchableOpacity>
);

export default DistrictListView;
