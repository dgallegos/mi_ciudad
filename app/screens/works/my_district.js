import React, {Component} from 'react';
import { View, Text, ListView,RefreshControl,Platform, Alert, Image, Dimensions, NetInfo,TouchableOpacity} from 'react-native';
import {Bars} from 'react-native-loader';
import Icon from 'react-native-vector-icons/FontAwesome';
//Components
import WorkListView from './work_list_view.js';
import * as Apis from "../../const/apis";
import moment from "moment";
//Language
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

export default class MyDistrict extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      ds: this.ds.cloneWithRows([]),
      isRefreshing:false,
      LoadingBar:true,
      position:{},
      initialPosition: '',
      lastPosition: '',
      isConnected: null
    }
  }

  componentDidMount() {
    if(!this.props.districts_id){
      navigator.geolocation.getCurrentPosition(
        (position) => {
          var initialPosition = JSON.stringify(position);
          const point = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          // console.log(point)
          this.setState({
            initialPosition,
            point
          });
          NetInfo.isConnected.addEventListener(
          'change',
              this._handleConnectivityChange.bind(this)
          );
          this.checkConnection()
        },
        (error) =>{this.setState({LoadingBar: false})} ,
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
      );

      this.watchID = navigator.geolocation.watchPosition((position) => {
        var lastPosition = JSON.stringify(position);
        this.setState({lastPosition});
      });
    }else{
      this.checkConnection();
      
      NetInfo.isConnected.addEventListener(
          'change',
          this._handleConnectivityChange.bind(this)
      );

      this.setState({
        ds: this.ds.cloneWithRows(this.props.works.reverse()),
        LoadingBar:false
      });
    }
  }

  checkConnection(){
    NetInfo.isConnected.fetch().done(
      isConnected => { 
        if(isConnected){
          this.getWorks(this.state.point).then(()=>{
            this.setState({LoadingBar:false})
          })
        }
        this.setState({isConnected}); 
      }
    );
  }

  _handleConnectivityChange(isConnected){
    if(isConnected){
      this.getWorks(this.state.point).then(()=>{
        this.setState({LoadingBar:false})
      });
    }
    this.setState({
      isConnected,
    });
  }


  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
    NetInfo.isConnected.removeEventListener(
        'change',
        this._handleConnectivityChange
    );
  }

  getWorks(point = {}){    
    let url = `${Apis.backoffice}api/district?flag=insidePolygon`;

    if(this.props.districts_id)
      url = `${Apis.backoffice}api/district/${this.props.districts_id}`;

    return fetch(url,{
        method: this.props.districts_id ? 'GET' : 'POST',
        headers:{
          "Content-Type":"application/json",
          "Accept":"application/json",
        },
        body: this.props.districts_id ? null : JSON.stringify({point}) 
      })
      .then(res=>res.json())
      .then(data=>{
        if(data.valid){
            this.setState({
              ds: this.ds.cloneWithRows(data.data.length ? data.data[0].works : [])
            });
            if(!this.props.districts_id)
              Alert.alert(I18n.t('found_district') + `: "${data.data[0].name}"` , null ,[{
                text: 'Aceptar'
              }]); 
        }else{
          Alert.alert(I18n.t('not_found_district'), null ,[{
            text: 'Aceptar'
          }]);
        }
      })
  }

  onRefresh(){
    this.setState({isRefreshing: true});
    this.getWorks(this.state.point)
    .then(()=>{
      this.setState({isRefreshing: false});
    })
  }


  render(){
    var MginT=(this.props.districts_id)?((Platform.OS=='android')?50:64):0;
    return(
      <View style={{flex: 1,marginTop:MginT}}>
        <View style={{position:'absolute',top: 0,left: 0,bottom: 0,right: 0}}>
          <Image style={{resizeMode:"cover", width: Dimensions.get('window').width, height: Dimensions.get('window').height}} source={require("../../assets/foto-sps-xs.png")}>
            <View style={{flex:1,backgroundColor:"rgba(0,184,255,0.7)"}}></View>
          </Image>
        </View>
        {
          !this.state.isConnected && 
            <View style={{flex: 1, alignItems: 'center', backgroundColor: 'transparent', justifyContent: 'center'}}>
              <Text style={{fontFamily: 'Gotham', color: '#fff', fontWeight: '600', fontSize: 22}}>{I18n.t('no_connection')}</Text>
              <TouchableOpacity onPress={()=>this.checkConnection()}>
                <Icon name='refresh' color='#fff' size={30}/>
              </TouchableOpacity>
            </View>
        }
        {
          this.state.LoadingBar && this.state.isConnected?
          <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
              <Bars size={10} color="#fff"/>
          </View> : 
            this.state.isConnected &&
            <ListView
              style={{flex: 1}}
              dataSource={this.state.ds}
              enableEmptySections={true}
              renderRow={(data)=><WorkListView data={data}/>}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isRefreshing}
                  onRefresh={this.onRefresh.bind(this)}
                  tintColor="#fff"
                  title="Loading more works"
                  titleColor="#fff"
                  colors={['#ff0000', '#00ff00', '#0000ff']}
                  progressBackgroundColor="#ffff00"
                />
              }
            /> 
        }
        { 
          (this.state.ds.rowIdentities[0].length == 0 && this.state.isConnected) &&
          <View style={{flex: 1, alignItems: 'center', backgroundColor: 'transparent'}}>
            <Text style={{fontFamily: 'Gotham', color: '#fff', fontWeight: '600', fontSize: 22}}>{I18n.t('no_works_in_district')}</Text>
          </View>
        }
      </View>
    )
  }
}
