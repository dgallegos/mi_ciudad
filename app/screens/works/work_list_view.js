import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as themeColors from '../../styles/colors.js';
var ResponsiveImage = require('react-native-responsive-image');
import {Actions} from 'react-native-router-flux';
import * as Apis from "../../const/apis";
import { getCorrectFontSize } from '../../components/multiresolution.js';

import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

const WorkListView = ({data}) => (
  <TouchableOpacity
    style={styles.workCard}
    onPress={()=>Actions.work_detail({data:data})}
  >
    <View style={{flex: 1}}>
      <ResponsiveImage
        source={{uri:`${Apis.backoffice}${data.images[0].path}`}}
        style={{height: 150, justifyContent:'flex-end', alignItems: 'stretch', borderRadius: 5}}
      >
        <View style={{backgroundColor: 'rgb(0,184,255)', height: 40, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{fontFamily: 'Gotham', color: 'white', fontSize: 12,textAlign:'center', fontWeight: '500'}}>{I18n.locale=="es"? data.name : data.name_english}</Text>
          <Text style={{fontFamily: 'Gotham', color: 'white', fontSize: 12,textAlign:'center'}}>{`${data.neighborhood} ${data.route}`}</Text>
        </View>
      </ResponsiveImage>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  workCard: {
    flex: 1,
    flexDirection: 'row',
    margin: 10,
    borderRadius: 10
  },
  cardContainer:{
    backgroundColor: 'transparent',
    flexDirection: 'row'
  },
  infoContainer: {
    flex: 1,
    marginLeft: 20
  },
  iconContainer: {
    justifyContent: 'center'
  },
  infoPrimaryText: {
    color: 'white',
    fontSize: 16
  },
  infoSecondaryText: {
    color: 'white',
    fontSize: 14
  },
  icon: {
    textAlign: 'center'
  }
})

export default WorkListView;
