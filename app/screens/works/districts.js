import React, {Component} from 'react';
import { View, Text, ListView, Image, Dimensions, RefreshControl, TouchableOpacity, NetInfo } from 'react-native';
import DistrictListView from './district_list_view.js';
import {Bars} from 'react-native-loader';
import _ from 'lodash';
import moment from "moment"
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
///Components
import * as Apis from "../../const/apis";
//Language
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

export default class Districts extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 ==! r2});
    this.state = {
      ds: this.ds.cloneWithRows([]),
      LoadingBar:true,
      isRefreshing:false,
      isConnected: null
    }
  }

  componentDidMount(){
    this.checkConnection();
  }


  checkConnection(){
    NetInfo.isConnected.fetch().done(
      isConnected => { 
        if(isConnected){
          this.getWorks().then(()=>{
            this.setState({LoadingBar:false})
          });
        }
        this.setState({isConnected}); 
      }
    );
  }

  getWorks(){
    return fetch(`${Apis.backoffice}api/district`)
    .then(res=>res.json())
    .then(data=>{
      _.forEach(data.data.sort((a,b)=> a.order - b.order),(data)=>{
        data.onProgress=0;
        data.done=0;
        _.forEach(data.works,(w)=>{
          if(w.status == 'Completado'){
            data.done++
          }else{
            data.onProgress++;
          }
        })
      })
      
      this.setState({ds: this.ds.cloneWithRows(data.data)})
      return;
    })
  }

  onRefresh(){
    if(this.state.isConnected){
      this.setState({isRefreshing: true});
      this.getWorks()
      .then(()=>{
        this.setState({isRefreshing: false});
      })
    }
  }

  render(){
    return(
      <View style={{flex: 1, backgroundColor: 'transparent'}}>
        {
          !this.state.isConnected && 
            <View style={{flex: 1, alignItems: 'center', backgroundColor: 'transparent', justifyContent: 'center'}}>
              <Text style={{fontFamily: 'Gotham', color: '#fff', fontWeight: '600', fontSize: 22}}>{I18n.t('no_connection')}</Text>
              <TouchableOpacity onPress={()=>this.checkConnection()}>
                <Icon name='refresh' color='#fff' size={30}/>
              </TouchableOpacity>
            </View>
        }
        {
          this.state.LoadingBar && this.state.isConnected ?
          <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
              <Bars size={10} color="#fff"/>
          </View> :
          this.state.isConnected && 
            <ListView
              dataSource={this.state.ds}
              enableEmptySections={true}
              renderRow={(data)=><DistrictListView data={data}/>}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isRefreshing}
                  onRefresh={this.onRefresh.bind(this)}
                  tintColor="#fff"
                  title="Loading"
                  titleColor="#fff"
                  colors={['#ff0000', '#00ff00', '#0000ff']}
                  progressBackgroundColor="#ffff00"
                />
              }
            />
        }
      </View>
    )
  }
}
