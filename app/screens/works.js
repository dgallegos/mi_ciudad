import React, {Component} from 'react';
import { View, Text, TouchableOpacity, ListView,Platform, Image, Dimensions } from 'react-native';
import {Actions} from 'react-native-router-flux';
import ScrollableTabView, {DefaultTabBar, ScrollableTabBar} from 'react-native-scrollable-tab-view';
import MyDistrict from './works/my_district.js';
import Districts from './works/districts.js';
import * as themeColors from '../styles/colors.js';

//Languaje
import I18n from 'react-native-i18n'
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

class Works extends Component{
  render(){
    return(
      <View style={{marginTop:((Platform.OS=='android')?50:64), flex: 1, backgroundColor: 'transparent'}}>
        <View style={{position:'absolute',top: 0,left: 0,bottom: 0,right: 0}}>
          <Image style={{resizeMode:"cover", width: Dimensions.get('window').width, height: Dimensions.get('window').height}} source={require("../assets/foto-sps-xs.png")}>
            <View style={{flex:1,backgroundColor:"rgba(0,184,255,0.7)"}}></View>
          </Image>
        </View>
        <ScrollableTabView
          style={{flex: 1, alignItems: 'stretch'}}
          renderTabBar={() =>
            <DefaultTabBar
              activeTextColor='#fff'
              inactiveTextColor='#fff'
              tabStyle={{ paddingBottom: 0, borderWidth: 0, backgroundColor: 'transparent'}}
              textStyle={{fontFamily: 'Gotham', fontWeight: 'normal', fontSize: 15}}
              underlineStyle={{ backgroundColor: '#fff'}}
              style={{height: 40, backgroundColor: 'rgb(0,184,255)', marginBottom: 10}}
            />
          }
          locked
        >   
            <MyDistrict tabLabel={I18n.t("in_my_district")}/>
            <Districts tabLabel={I18n.t("districts")}/>
        </ScrollableTabView>
      </View>
    )
  }

}

export default Works;
