import React, {Component} from 'react';
import { View, Text, TouchableOpacity, ListView, Image,RefreshControl, Platform, Dimensions, NetInfo } from 'react-native';
import {Actions} from 'react-native-router-flux';
//node_modules
var ResponsiveImage = require('react-native-responsive-image');
import {Bars} from 'react-native-loader';
import Icon from 'react-native-vector-icons/FontAwesome';
//Components
import * as Apis from "../const/apis";
import moment from "moment";

//Language
import I18n from 'react-native-i18n'
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;


class News extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      ds: this.ds.cloneWithRows([]),
      isRefreshing:false,
      LoadingBar:true,
      isConnected: null
    }
  }

  translateDate(){
    console.log(I18n.locale)
  }

  componentDidMount(){
    NetInfo.isConnected.addEventListener(
        'change',
        this._handleConnectivityChange.bind(this)
    );

    this.checkConnection();
  }

  checkConnection(){
    NetInfo.isConnected.fetch().done(
      isConnected => { 
        if(isConnected){
          this.getNews().then(()=>{
            this.setState({LoadingBar:false})
          });
        }
        this.setState({isConnected}); 
      }
    );
  }

  _handleConnectivityChange(isConnected){
    if(isConnected){
      this.getNews().then(()=>{
        this.setState({LoadingBar:false})
      });
    }
    this.setState({
      isConnected,
    });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'change',
        this._handleConnectivityChange
    );
  }


  getNews(){
    return fetch(`${Apis.backoffice}api/news`)
    .then(res=>res.json())
    .then(data=>{
      this.setState({
        ds: this.ds.cloneWithRows(data.data.reverse())
      });
      return;
    })
  }

  renderRow(data, rowID){
    return (
      <TouchableOpacity
        style={{width: rowID == 0 ? Dimensions.get('window').width * 0.97 : Dimensions.get('window').width * 0.47, height: 180, marginBottom: 8, padding: 0, marginHorizontal: 4,  }}
        onPress={() => Actions.news_detail({data:data})}
      >
        <Image
            source={{uri:`${Apis.backoffice}${data.images.hasOwnProperty(0) ? data.images[0].path: null}`}}
            style={{height: 120,justifyContent:'flex-end', backgroundColor: 'transparent', flex: 1, width: null, borderRadius: 5}}
          >
          <View style={{backgroundColor: '#265fa6', padding: 10, height: 40, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{fontFamily: 'Gotham',color: 'white', fontSize: 12, height: 30, textAlign:'center'}} ellipsizeMode='tail'>
              {I18n.locale=="es"? data.title : data.title_english}
            </Text>
          </View>
        </Image>
      </TouchableOpacity>
    )
  }


  onRefresh(){
    this.setState({isRefreshing: true});
    this.getNews()
    .then(()=>{
      this.setState({isRefreshing: false});
    })
  }

  render(){
    return(
      <View style={{flex: 1, marginTop: (Platform.OS=='android') ? 50 : 64,}}>
        {
          !this.state.isConnected && 
            <View style={{flex: 1, alignItems: 'center', backgroundColor: 'transparent', justifyContent: 'center'}}>
              <Text style={{fontFamily: 'Gotham', color: '#265fa6', fontWeight: '600', fontSize: 22}}>{I18n.t('no_connection')}</Text>
              <TouchableOpacity onPress={()=>this.checkConnection()}>
                <Icon name='refresh' color='#265fa6' size={30}/>
              </TouchableOpacity>
            </View>
        }
        {
          this.state.LoadingBar && this.state.isConnected?
            <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
                <Bars size={10} color='#265fa6'/>
            </View> :
            this.state.isConnected && 
              <View style={{flex: 1}}>
                <ListView
                  contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap', marginTop: 10}}
                  style={{flex: 1}}
                  dataSource={this.state.ds}
                  enableEmptySections={true}
                  renderRow={(row, _, rowID) => this.renderRow(row,rowID)}
                  refreshControl={
                    <RefreshControl
                      refreshing={this.state.isRefreshing}
                      onRefresh={this.onRefresh.bind(this)}
                      tintColor="#265fa6"
                      title="Loading more good news"
                      titleColor="#265fa6"
                      colors={['#ff0000', '#00ff00', '#0000ff']}
                      progressBackgroundColor="#ffff00"
                    />
                  }
                />
              </View>
        }
      </View>
    )
  }

}

export default News;
