const formatCurrency = (value,currency)=>{
  value =  parseFloat(value)
  if(value < 1){
    var num = `${currency} ${value.toFixed(2)}`
    return num.toString()
  }
  if(typeof value === "number" && value > 1){
    var num = `${currency} ${value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}`
    return num.toString()
  }
  return '';
}

export default formatCurrency
